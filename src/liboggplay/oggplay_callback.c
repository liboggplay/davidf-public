/*
   Copyright (C) 2003 Commonwealth Scientific and Industrial Research
   Organisation (CSIRO) Australia

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

   - Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

   - Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

   - Neither the name of CSIRO Australia nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
   PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE ORGANISATION OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
 * oggplay_callback.c
 *
 * Shane Stephens <shane.stephens@annodex.net>
 */
#include "oggplay_private.h"

#define TIME_THEORA_DECODE 0

#include <stdlib.h>
#if TIME_THEORA_DECODE
#include <sys/time.h>
#endif
#include <time.h>
#include <string.h>

void
oggplay_init_theora(void *user_data) {

  OggPlayTheoraDecode   * decoder     = (OggPlayTheoraDecode *)user_data;

  theora_info_init(&(decoder->video_info));
  theora_comment_init(&(decoder->video_comment));
  decoder->remaining_header_packets = 3;
  decoder->vdecoder.granulepos_seen = 0;
  decoder->vdecoder.frame_delta = 0;
  decoder->vdecoder.y_width = 0;
  decoder->vdecoder.fps_num = 0;
  decoder->vdecoder.fps_denom = 0;
  decoder->vdecoder.decoder.decoded_type = OGGPLAY_YUV_VIDEO;
  decoder->vdecoder.convert_to_rgb = 0;
}

void
oggplay_shutdown_theora(void *user_data) {

  OggPlayTheoraDecode   * decoder = (OggPlayTheoraDecode *)user_data;

  if (decoder->remaining_header_packets == 0) {
    theora_clear(&(decoder->video_handle));
  }
  theora_info_clear(&(decoder->video_info));
  theora_comment_clear(&(decoder->video_comment));
}

int
oggplay_callback_theora (OGGZ * oggz, ogg_packet * op, long serialno,
                void * user_data) {

  OggPlayTheoraDecode   * decoder     = (OggPlayTheoraDecode *)user_data;
  OggPlayDecode         * common      = &(decoder->vdecoder.decoder);
  OggPlayCommonVideoDecode * vcommon  = &(decoder->vdecoder);
  ogg_int64_t             granulepos  = oggz_tell_granulepos(oggz);
  yuv_buffer              buffer;
  int granuleshift;
  long frame;

#if TIME_THEORA_DECODE
  struct timeval          tv;
  struct timeval          tv2;
  int                     musec;
#endif

  /*
   * always decode headers
   */
  if (theora_packet_isheader(op)) {
    if (theora_decode_header(&(decoder->video_info), &(decoder->video_comment), op) < 0)
      return -1;

    /*
     * initialise width/stride/height data (this is common to all frames).
     * Use the buffer stride for the width to avoid passing negative stride
     * issues on to the user.
     */
    vcommon->y_width = vcommon->y_stride = decoder->video_info.frame_width;
    vcommon->y_height = decoder->video_info.frame_height;
    vcommon->uv_width = vcommon->uv_stride = decoder->video_info.frame_width / 2;
    vcommon->uv_height = decoder->video_info.frame_height / 2;
    vcommon->fps_num = decoder->video_info.fps_numerator;
    vcommon->fps_denom = decoder->video_info.fps_denominator;

    if (--(decoder->remaining_header_packets) == 0) {
      theora_decode_init(&(decoder->video_handle), &(decoder->video_info));
    }
    return 0;
  }
  else if (decoder->remaining_header_packets != 0) {
    /*
     * Invalid Ogg file. Missing headers
     *
     */
    return -1;
  }

  if (!decoder->vdecoder.decoder.active) {
    /*
     * don't decode other packets
     */
    return 0;
  }

  /*
   * if we get to here then we've passed all the header packets
   */
  if (common->current_loc == -1)
    common->current_loc = 0;

  /*
   * Decode the frame
   */

#if TIME_THEORA_DECODE
  gettimeofday(&tv, NULL);
#endif

  if (theora_decode_packetin(&(decoder->video_handle), op) < 0)
    return -1;

  if (theora_decode_YUVout(&(decoder->video_handle), &buffer) < 0)
    return -1;

#if TIME_THEORA_DECODE
  gettimeofday(&tv2, NULL);
  musec = tv2.tv_usec - tv.tv_usec;
  if (tv2.tv_sec > tv.tv_sec)
    musec += (tv2.tv_sec - tv.tv_sec) * 1000000;
  printf("decode took %dus\n", musec);
#endif

  if (granulepos != -1) {
    granuleshift = oggz_get_granuleshift(oggz, serialno);
    frame = (granulepos >> granuleshift);
    frame += (granulepos & ((1 << granuleshift) - 1));
    common->current_loc = frame * common->granuleperiod;
  } else {
    common->current_loc = -1;
  }

  if
  (
    (common->current_loc == -1)
    ||
    (common->current_loc >= common->player->presentation_time)
  )
  {
    /*
     * store the frame,
     * use the buffer stride for the width to avoid passing negative stride
     * issues on to the user.
     * */
    oggplay_data_handle_theora_frame(decoder, &buffer);
  }

  if (op->e_o_s) {
    common->active = 0;
    common->player->active_tracks--;
  }

  return 0;

}

void
oggplay_init_cmml (void * user_data) {

  OggPlayCmmlDecode * decoder = (OggPlayCmmlDecode *)user_data;
  decoder->decoder.decoded_type = OGGPLAY_CMML;
  decoder->granuleshift = 32; /* default */
}

int
oggplay_callback_cmml (OGGZ * oggz, ogg_packet * op, long serialno,
                void * user_data) {

  OggPlayCmmlDecode * decoder     = (OggPlayCmmlDecode *)user_data;
  OggPlayDecode     * common      = &(decoder->decoder);
  ogg_int64_t         granulepos  = oggz_tell_granulepos (oggz);

  if (granulepos == 0) {
    if (memcmp(op->packet, "CMML\0\0\0\0", 8) == 0) {
      decoder->granuleshift = op->packet[28];
    }
  } else {

    if (decoder->granuleshift > 0) {
      granulepos >>= decoder->granuleshift;
    }

    common->current_loc = granulepos * common->granuleperiod;

    oggplay_data_handle_cmml_data (&(decoder->decoder), op->packet, op->bytes);
  }

  return 0;

}

void
oggplay_init_skel (void * user_data) {

  OggPlaySkeletonDecode * decoder = (OggPlaySkeletonDecode *)user_data;

  decoder->presentation_time = 0;
  decoder->base_time = 0;
}

static inline unsigned long extract_int32(unsigned char *data) {
  return data[0] | (data[1] << 8) | (data[2] << 16) | (data[3] << 24);
}

static inline ogg_int64_t extract_int64(unsigned char *data) {
  return ((ogg_int64_t)(extract_int32(data))) |
         (((ogg_int64_t)(extract_int32(data + 4))) << 32);
}

int
oggplay_callback_skel (OGGZ * oggz, ogg_packet * op, long serialno,
                void * user_data) {

  OggPlaySkeletonDecode * decoder = (OggPlaySkeletonDecode *)user_data;

  if (strncmp((char *)op->packet, "fishead", 7) == 0) {
    ogg_int64_t pt_num, pt_den, bt_num, bt_den;

    pt_num = extract_int64(op->packet + 12);
    pt_den = extract_int64(op->packet + 20);
    bt_num = extract_int64(op->packet + 28);
    bt_den = extract_int64(op->packet + 36);

    if (pt_den != 0) {
      decoder->presentation_time = OGGPLAY_TIME_INT_TO_FP(pt_num) / pt_den;
    } else {
      decoder->presentation_time = 0;
    }
    if (bt_den != 0) {
      decoder->base_time = OGGPLAY_TIME_INT_TO_FP(bt_num) / bt_den;
    } else {
      decoder->base_time = 0;
    }

    /* initialise the presentation times in the player to the values recorded in the skeleton */
    decoder->decoder.player->presentation_time = decoder->presentation_time;
  } else {
    int i;
    long          preroll       = extract_int32(op->packet + 44);
    long          serialno      = extract_int32(op->packet + 12);
    //ogg_int64_t   start_granule = extract_int64(op->packet + 36);

    for (i = 1; i < decoder->decoder.player->num_tracks; i++) {
      if (decoder->decoder.player->decode_data[i]->serialno == serialno) {
        decoder->decoder.player->decode_data[i]->preroll = preroll;
        break;
      }
    }
  }

  return 0;

}

int
oggplay_fish_sound_callback_floats(FishSound * fsound, float ** pcm, 
                                          long frames, void *user_data) {

  OggPlayAudioDecode *decoder = (OggPlayAudioDecode *)user_data;
  OggPlayDecode *common = &(decoder->decoder);

  /*
   * calculate the current location here so that it's only updated when
   * audio data is actually available for processing
   */
  if (common->last_granulepos > 0) {
    common->current_loc = common->last_granulepos * common->granuleperiod;
  } else {
    common->current_loc = -1;
  }

  if
  (
    (common->current_loc == -1)
    ||
    (common->current_loc >= common->player->presentation_time)
  )
  {


    /*
     * store the frame
     */
    oggplay_data_handle_audio_data(&(decoder->decoder), (short *)pcm, frames,
              sizeof(float));

      return FISH_SOUND_STOP_ERR;
  }

  return FISH_SOUND_CONTINUE;
}

void
oggplay_init_audio (void * user_data) {

  OggPlayAudioDecode  * decoder = (OggPlayAudioDecode *)user_data;

  decoder->sound_handle = fish_sound_new(FISH_SOUND_DECODE,
                                                      &(decoder->sound_info));

  decoder->sound_info.channels = 0;
  fish_sound_set_interleave(decoder->sound_handle, 1);
  fish_sound_set_decoded_float_ilv(decoder->sound_handle,
                                      oggplay_fish_sound_callback_floats,
                                      (void *)decoder);

  decoder->decoder.decoded_type = OGGPLAY_FLOATS_AUDIO;
}

void
oggplay_shutdown_audio(void *user_data) {

  OggPlayAudioDecode   * decoder = (OggPlayAudioDecode *)user_data;

  fish_sound_delete(decoder->sound_handle);

}

int
oggplay_callback_audio (OGGZ * oggz, ogg_packet * op, long serialno,
                void * user_data) {

  OggPlayAudioDecode   * decoder     = (OggPlayAudioDecode *)user_data;
  OggPlayDecode        * common      = &(decoder->decoder);
  ogg_int64_t            granulepos  = oggz_tell_granulepos(oggz);

  if (granulepos > 0 && (!decoder->decoder.active)) {
    return 0;
  }

  common->last_granulepos = granulepos;

  fish_sound_prepare_truncation (decoder->sound_handle, op->granulepos,
                                                                op->e_o_s);
  fish_sound_decode (decoder->sound_handle, op->packet, op->bytes);

  if (decoder->sound_info.channels == 0) {
    fish_sound_command(decoder->sound_handle, FISH_SOUND_GET_INFO,
                    &(decoder->sound_info), sizeof(FishSoundInfo));
  }

  if (op->e_o_s) {
    common->active = 0;
    common->player->active_tracks--;
  }

  return 0;
}

void
oggplay_init_kate(void *user_data) {

#ifdef HAVE_KATE
  int ret;
  OggPlayKateDecode   * decoder     = (OggPlayKateDecode *)user_data;

  decoder->init = 0;
  ret = kate_high_decode_init(&(decoder->k));
  if (ret < 0) {
    /* what to do ? */
  }
  else {
    decoder->init = 1;
  }
  decoder->decoder.decoded_type = OGGPLAY_KATE;

#ifdef HAVE_TIGER
  decoder->use_tiger = 1;
  decoder->overlay_dest = -1;

  ret = tiger_renderer_create(&(decoder->tr));
  if (ret < 0) {
    /* what to do ? */
    decoder->tr = NULL;
  }
  if (decoder->use_tiger) {
    decoder->decoder.decoded_type = OGGPLAY_RGBA_VIDEO;
  }
#endif

#endif
}

void
oggplay_shutdown_kate(void *user_data) {

#ifdef HAVE_KATE
  OggPlayKateDecode   * decoder = (OggPlayKateDecode *)user_data;

#ifdef HAVE_TIGER
  if (decoder->tr) {
    tiger_renderer_destroy(decoder->tr);
  }
#endif

  if (decoder->init) {
    kate_high_decode_clear(&(decoder->k));
  }
#endif
}

int
oggplay_callback_kate (OGGZ * oggz, ogg_packet * op, long serialno,
                void * user_data) {

#ifdef HAVE_KATE
  OggPlayKateDecode     * decoder     = (OggPlayKateDecode *)user_data;
  OggPlayDecode         * common      = &(decoder->decoder);
  ogg_int64_t             granulepos  = oggz_tell_granulepos(oggz);
  int                     granuleshift;
  ogg_int64_t             base, offset;
  kate_packet kp;
  const kate_event *ev = NULL;
  int ret;

  if (!decoder->init) {
    return E_OGGPLAY_UNINITIALISED;
  }

  kate_packet_wrap(&kp, op->bytes, op->packet);
  ret = kate_high_decode_packetin(&(decoder->k), &kp, &ev);
  if (ret < 0) {
    return E_OGGPLAY_BAD_INPUT;
  }

  if (granulepos != -1) {
    granuleshift = oggz_get_granuleshift(oggz, serialno);
    base = (granulepos >> granuleshift);
    offset = granulepos - (base << granuleshift);
    common->current_loc = (base+offset) * common->granuleperiod;
  } else {
    common->current_loc = -1;
  }

  if
  (
    (common->current_loc == -1)
    ||
    (common->current_loc >= common->player->presentation_time)
  )
  {
    /*
     * process the data from the packet
     * */
    if (ev) {
      oggplay_data_handle_kate_data(decoder, ev);
    }
  }

  if (op->e_o_s) {
    common->active = 0;
  }

#endif

  return 0;

}

void
oggplay_init_dirac(void *user_data) {

#ifdef HAVE_SCHRO
  OggPlayDiracDecode   * decoder     = (OggPlayDiracDecode *)user_data;

  /* First of all initialize the supporting libraries */
  schro_init();

  decoder->video_handle = schro_decoder_new();
  decoder->vdecoder.granulepos_seen = 0;
  decoder->vdecoder.frame_delta = 0;
  decoder->vdecoder.y_width = 0;
  decoder->vdecoder.fps_num = 0;
  decoder->vdecoder.fps_denom = 0;
  decoder->vdecoder.decoder.decoded_type = OGGPLAY_YUV_VIDEO;
  decoder->vdecoder.convert_to_rgb = 0;
#endif
}

void
oggplay_reset_dirac(void *user_data) {

#ifdef HAVE_SCHRO
  OggPlayDiracDecode   * decoder = (OggPlayDiracDecode *)user_data;

  schro_decoder_reset(decoder->video_handle);
#endif
}

void
oggplay_shutdown_dirac(void *user_data) {

#ifdef HAVE_SCHRO
  OggPlayDiracDecode   * decoder = (OggPlayDiracDecode *)user_data;

  schro_decoder_free(decoder->video_handle);
  free(decoder->video_info);
#endif
}

struct ot_dirac_gpos {
  ogg_uint32_t pt;
  ogg_uint16_t dist;
  ogg_uint16_t delay;
  ogg_int64_t dt;
};

static void
ot_dirac_gpos_parse (ogg_int64_t iframe, ogg_int64_t pframe,
                     struct ot_dirac_gpos * dg)
{
  dg->pt = (iframe + pframe) >> 9;
  dg->dist = ((iframe & 0xff) << 8) | (pframe & 0xff);
  dg->delay = pframe >> 9;
  dg->dt = (ogg_int64_t)dg->pt - dg->delay;
}

int
oggplay_callback_dirac (OGGZ * oggz, ogg_packet * op, long serialno,
                void * user_data) {

#ifdef HAVE_SCHRO
  OggPlayDiracDecode       * decoder     = (OggPlayDiracDecode *)user_data;
  OggPlayCommonVideoDecode * vcommon     = &(decoder->vdecoder);
  OggPlayDecode            * common      = &(decoder->vdecoder.decoder);
  ogg_int64_t                granulepos  = oggz_tell_granulepos(oggz);

  if (common->current_loc == -1) {
    common->current_loc = 0;
  }

  if (op->bytes) {
    SchroBuffer *schro_buf;
    schro_buf = schro_buffer_new_and_alloc(op->bytes);
    if (schro_buf) {
      memcpy(schro_buf->data, op->packet, op->bytes);

      if (op->granulepos != UINT64_MAX) {
        ogg_int64_t *granulepos_tag = malloc(sizeof(*granulepos_tag));
        if (granulepos_tag) {
          struct ot_dirac_gpos dg;
          ot_dirac_gpos_parse(granulepos >> 22, granulepos & ((1<<22)-1), &dg);
          *granulepos_tag = dg.pt * common->granuleperiod;
          schro_buf->tag = schro_tag_new(granulepos_tag, free);
        }
      }

      schro_decoder_autoparse_push(decoder->video_handle, schro_buf);
    }
  }

  if (op->e_o_s) {
    schro_decoder_autoparse_push_end_of_sequence(decoder->video_handle);
  }

  /*
   * Decode the frame
   */
  while (1) {
    int state = schro_decoder_autoparse_wait(decoder->video_handle);

    switch(state) {
      case SCHRO_DECODER_NEED_BITS:
        if (op->e_o_s) {
          common->active = 0;
          common->player->active_tracks--;
        }
        return 0;

      case SCHRO_DECODER_FIRST_ACCESS_UNIT:
        decoder->last_good_pnum = 0;
        decoder->last_good_loc = -1;
        decoder->video_info = schro_decoder_get_video_format(decoder->video_handle);
        vcommon->y_width = vcommon->y_stride = decoder->video_info->width;
        vcommon->y_height = decoder->video_info->height;
        vcommon->fps_num = decoder->video_info->frame_rate_numerator;
        vcommon->fps_denom = decoder->video_info->frame_rate_denominator;

        switch (decoder->video_info->chroma_format) {
          case SCHRO_CHROMA_420:
            vcommon->uv_width = vcommon->uv_stride = decoder->video_info->width/2;
            vcommon->uv_height = decoder->video_info->height/2;
            decoder->frame_format = SCHRO_FRAME_FORMAT_U8_420;
            break;
          case SCHRO_CHROMA_422:
            vcommon->uv_width = vcommon->uv_stride = decoder->video_info->width/2;
            vcommon->uv_height = decoder->video_info->height;
            decoder->frame_format = SCHRO_FRAME_FORMAT_U8_422;
            break;
          case SCHRO_CHROMA_444:
            vcommon->uv_width = vcommon->uv_stride = decoder->video_info->width;
            vcommon->uv_height = decoder->video_info->height;
            decoder->frame_format = SCHRO_FRAME_FORMAT_U8_444;
            break;
          default:
            return -1; /* Should not reach here!!*/
        }
        break;

      case SCHRO_DECODER_NEED_FRAME: {
        SchroFrame *schro_frame;
        schro_frame = schro_frame_new_and_alloc(NULL, decoder->frame_format,
                                                vcommon->y_width,
                                                vcommon->y_height);
        if (!schro_frame) {
          return -1;
        }

        schro_decoder_add_output_picture(decoder->video_handle, schro_frame);
        break;
      }

      case SCHRO_DECODER_OK: {
        SchroTag *schro_tag = schro_decoder_get_picture_tag(decoder->video_handle);
        SchroPictureNumber pnum = schro_decoder_get_picture_number(decoder->video_handle);
        SchroFrame *schro_frame = schro_decoder_pull(decoder->video_handle);

        if (schro_frame->width == 0 || schro_frame->height == 0) {
          /* A placeholder picture with no data. Discard */
          if (schro_tag) {
            schro_tag_free(schro_tag);
          }
          schro_frame_unref(schro_frame);
          break;
        }

        if (schro_tag) {
          common->current_loc = *(ogg_int64_t*)schro_tag->value;
          schro_tag_free(schro_tag);
          decoder->last_good_pnum = pnum;
          decoder->last_good_loc = common->current_loc;
        } else if (decoder->last_good_loc != -1) {
          /* no provided granuleposition for this frame, interpolate */
          common->current_loc = decoder->last_good_loc
                               + (pnum - decoder->last_good_pnum)
                               * common->granuleperiod
                               * (decoder->video_info->interlaced_coding ? 1 : 2);
        }

        if (common->current_loc >= common->player->presentation_time) {
          /* only store frames that are in the future */
          oggplay_data_handle_dirac_frame(decoder, schro_frame);
        }
        schro_frame_unref(schro_frame);
        return 0;
      }

      case SCHRO_DECODER_ERROR:
        return -1;
    }
  }
#endif

  return 0;
}

OggPlayCallbackFunctions callbacks[] = {
  {oggplay_init_theora,
   oggplay_callback_theora,
   NULL,
   oggplay_shutdown_theora,
   sizeof(OggPlayTheoraDecode)},        /* THEORA */
  {oggplay_init_audio,
   oggplay_callback_audio,
   NULL,
   oggplay_shutdown_audio,
   sizeof(OggPlayAudioDecode)},         /* VORBIS */
  {oggplay_init_audio,
   oggplay_callback_audio,
   NULL,
   oggplay_shutdown_audio,
   sizeof(OggPlayAudioDecode)},         /* SPEEX */
  {NULL, NULL, NULL, NULL, sizeof(OggPlayDecode)}, /* PCM */
  {oggplay_init_cmml,
   oggplay_callback_cmml,
   NULL,
   NULL,
   sizeof(OggPlayCmmlDecode)},
  {NULL, NULL, NULL, NULL, sizeof(OggPlayDecode)}, /* ANX2 */
  {oggplay_init_skel,
   oggplay_callback_skel,
   NULL,
   NULL,
   sizeof(OggPlaySkeletonDecode)},
  {NULL, NULL, NULL, NULL, sizeof(OggPlayDecode)}, /* FLAC0 */
  {NULL, NULL, NULL, NULL, sizeof(OggPlayDecode)}, /* FLAC */
  {NULL, NULL, NULL, NULL, sizeof(OggPlayDecode)}, /* ANXDATA */
  {NULL, NULL, NULL, NULL, sizeof(OggPlayDecode)}, /* CELT */
  {oggplay_init_kate,
   oggplay_callback_kate,
   NULL,
   oggplay_shutdown_kate,
   sizeof(OggPlayKateDecode)},          /* KATE */
  {oggplay_init_dirac,
   oggplay_callback_dirac,
   oggplay_reset_dirac,
   oggplay_shutdown_dirac,
   sizeof(OggPlayDiracDecode)},          /* DIRAC */
  {NULL, NULL, NULL, NULL, sizeof(OggPlayDecode)}  /* UNKNOWN */
};

OggPlayDecode *
oggplay_initialise_decoder(OggPlay *me, int content_type, int serialno) {

  ogg_int64_t    num;
  ogg_int64_t    denom;
  OggPlayDecode *decoder = NULL;

  if (me == NULL)
    return NULL;

  decoder = oggplay_malloc (callbacks[content_type].size);

  if (decoder == NULL)
    return NULL;

  decoder->serialno = serialno;
  decoder->content_type = content_type;
  decoder->content_type_name =
          oggz_stream_get_content_type (me->oggz, serialno);
  decoder->active = 1;
  decoder->final_granulepos = -1;
  decoder->player = me;
  decoder->decoded_type = OGGPLAY_TYPE_UNKNOWN;

  /*
   * set the StreamInfo to unitialised until we get some real data in
   */
  decoder->stream_info = OGGPLAY_STREAM_UNINITIALISED;

  /*
   * set to -1 until headers decoded
   */
  decoder->current_loc = -1;
  decoder->last_granulepos = 0;

  /*
   * the offset is how far advanced or delayed this track is to the "standard"
   * time position.  An offset of 1000, for example, indicates that data for
   * this track arrives 1 second in advance of data for other tracks
   */
  decoder->offset = 0;

  oggz_get_granulerate(me->oggz, serialno, &num, &denom);

  /*
   * convert num and denom to a 32.32 fixed point value
   */
  if (num != 0) {
    decoder->granuleperiod = OGGPLAY_TIME_INT_TO_FP(denom) / num;
  } else {
    decoder->granuleperiod = 0;
  }

  if (callbacks[content_type].init != NULL) {
    callbacks[content_type].init(decoder);
  }

  oggplay_data_initialise_list(decoder);

  return decoder;
}


/*
 * this function needs to be called on each track to clear up allocated memory
 */
void
oggplay_callback_shutdown(OggPlayDecode *decoder) {

  if (callbacks[decoder->content_type].shutdown != NULL) {
    callbacks[decoder->content_type].shutdown(decoder);
  }

  oggplay_data_shutdown_list(decoder);

  oggplay_free(decoder);
}

/*
 * after a seek, some decoders would like to be notified for state resets
 */
void
oggplay_callback_reset(OggPlayDecode *decoder) {

  if (callbacks[decoder->content_type].reset != NULL) {
    callbacks[decoder->content_type].reset(decoder);
  }
}


/*
 * this is the callback that is used before all track types have been
 * determined - i.e. at the beginning of an ogg bitstream or at the start
 * of a new chain
 */
int
oggplay_callback_predetected (OGGZ *oggz, ogg_packet *op, long serialno,
                void *user_data) {

  OggPlay     * me;
  int           i;
  int           content_type  = 0;

  me = (OggPlay *)user_data;
  content_type = oggz_stream_get_content (me->oggz, serialno);

  /*
   * if we encounter a serialno for the second time, then we've reached the
   * end of the b_o_s packets
   */
  for (i = 0; i < me->num_tracks; i++) {
    if (serialno == me->decode_data[i]->serialno) {

      me->all_tracks_initialised = 1;

      /*
       * call appropriate callback
       */
      if (callbacks[content_type].callback != NULL) {
        callbacks[content_type].callback(oggz, op, serialno,
                                          me->decode_data[i]);
      }

      /*
       * set up all the other callbacks
       */
      for (i = 0; i < me->num_tracks; i++) {
        serialno = me->decode_data[i]->serialno;
        content_type = oggz_stream_get_content (me->oggz, serialno);
        oggz_set_read_callback(me->oggz, serialno,
                        callbacks[content_type].callback, me->decode_data[i]);
      }

      /*
       * destroy this callback
       */
      oggz_set_read_callback (me->oggz, -1, NULL, NULL);

      return 0;
    }
  }

  me->callback_info = oggplay_realloc (me->callback_info,
                  sizeof (OggPlayCallbackInfo) * ++me->num_tracks);
  if (me->callback_info == NULL)
    return -1;

  me->decode_data = oggplay_realloc (me->decode_data, sizeof (long) * me->num_tracks);
  if (me->decode_data == NULL)
    return -1;

  me->decode_data[me->num_tracks - 1] = oggplay_initialise_decoder(me,
                                                      content_type, serialno);
  if (me->decode_data[me->num_tracks - 1] == NULL)
    return -1; 

  /*me->decode_data->callback_info = me->callback_info + (me->num_tracks - 1);*/

  /*
   * call appropriate callback
   */
  if (callbacks[content_type].callback != NULL) {
    callbacks[content_type].callback(oggz, op, serialno,
                                          me->decode_data[me->num_tracks - 1]);
  }

  return 0;

}
